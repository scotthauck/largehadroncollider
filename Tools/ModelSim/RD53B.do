vlib work

# Xilinix Libraries
#vmap unifast  C:/Xilinx/simulation_libs/unifast
#vmap unimacro C:/Xilinx/simulation_libs/unimacro
#vmap unisim   C:/Xilinx/simulation_libs/unisim
#vmap secureip C:/Xilinx/simulation_libs/secureip

#RD53B Modules
vlog -work work ../../Src/RD53_top.sv
vcom -work work ../../Src/blink.vhd
vlog -work work ../../Src/ttc_top.v
vlog -work work ../../Src/SRL.v
vlog -work work ../../Src/shift_align.v
vlog -work work ../../Src/chip_output.sv
vlog -work work ../../Src/trigger_timing.v
vlog -work work ../../Src/command_process.sv
vlog -work work ../../Src/hit_generator3.sv
vlog -work work ../../Src/hitMaker3.sv
vlog -work work ../../Src/command_out.sv
vlog -work work ../../Src/frame_buffer_four_lane.sv
vlog -work work ../../Src/aurora_tx_four_lane.sv
vcom -work work ../../Src/yarr_files/TX/aurora_tx_lane128.vhd
vlog -work work ../../Src/scrambler_dummy.v
vcom -work work ../../Src/yarr_files/TX/gearbox66to32.vhd
vcom -work work ../../Src/yarr_files/TX/serdes32to8.vhd
#vcom -work work ./serdes8to1_nosynth.vhd
vcom -work work ../../Src/yarr_files/TX/serdes8to1.vhd

vlog -work work ../../Src/RD53B_const_io_encode.v
vlog -work work ../../Src/RD53B_const_regmap.v

#testing files
vcom -work work ../../Testbench/rd53b_fpga_pkg.vhd
vcom -work work ../../Testbench/rom_128x64bit_pkg.vhd
vcom -work work ../../Testbench/fifo_Nx16bit.vhd
vcom -work work ../../Testbench/model_yarr_ttc.vhd
#vlog -work work ../../Testbench/event_data_recorder.sv
vcom -2008 -work work ../../Testbench/model_yarr_rcv.vhd
vcom -work work ../../Testbench/rd53b_regmap_pkg.vhd
vcom -work work ../../Testbench/rd53b_tb_pkg.vhd

#Xilinx IP cores
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/clk_wiz_SLAC/clk_wiz_SLAC_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/fifo_generator_0/fifo_generator_0_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/triggerFifo/triggerFifo_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/triggerTagFifo/triggerTagFifo_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/hitDataFIFO/hitDataFIFO_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/rom_128x64bit/rom_128x64bit_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/fifo_generator_1/fifo_generator_1_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/fifo_generator_2/fifo_generator_2_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/fifo_one_to_four/fifo_one_to_four_sim_netlist.vhdl


#XILINX ILA IPs
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/ila_command_received/ila_command_received_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/ila_output_data_top_level/ila_output_data_top_level_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/ila_ttc/ila_ttc_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/ila_chip_out/ila_chip_out_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/ila_command_process/ila_command_process_sim_netlist.vhdl
vcom -work work ../../Tools/Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/ila_hitgen_fifo/ila_hitgen_fifo_sim_netlist.vhdl



vlog -work work ../../Src/on_chip_top_SLAC.sv
vcom -work work ../../Testbench/tb_rd53b_emulator.vhd

vsim -t 1fs -novopt tb_rd53b_emulator -L unisim -L secureip -L unifast -L unimacro

view signals
view wave

do wave_RD53B.do

run -all