
################################################################
# This is a generated script based on design: ps1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2017.4
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source ps1_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7k160tfbg484-2
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name ps1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set dip_switches_4bits [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 dip_switches_4bits ]
  set gpio1 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 gpio1 ]
  set gpio2 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 gpio2 ]
  set push_buttons_5bits [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gpio_rtl:1.0 push_buttons_5bits ]
  set uart [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:uart_rtl:1.0 uart ]

  # Create ports
  set clk [ create_bd_port -dir I -type clk clk ]
  set pit1_toggle [ create_bd_port -dir O pit1_toggle ]
  set reset_n [ create_bd_port -dir I -type rst reset_n ]
  set_property -dict [ list \
   CONFIG.POLARITY {ACTIVE_HIGH} \
 ] $reset_n

  # Create instance: mcs0, and set properties
  set mcs0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze_mcs:3.0 mcs0 ]
  set_property -dict [ list \
   CONFIG.DEBUG_ENABLED {1} \
   CONFIG.GPI1_SIZE {32} \
   CONFIG.GPI2_SIZE {32} \
   CONFIG.GPI3_SIZE {4} \
   CONFIG.GPI4_SIZE {5} \
   CONFIG.GPIO1_BOARD_INTERFACE {Custom} \
   CONFIG.GPIO2_BOARD_INTERFACE {Custom} \
   CONFIG.GPIO3_BOARD_INTERFACE {Custom} \
   CONFIG.GPIO4_BOARD_INTERFACE {Custom} \
   CONFIG.JTAG_CHAIN {1} \
   CONFIG.MEMSIZE {32768} \
   CONFIG.RESET_BOARD_INTERFACE {Custom} \
   CONFIG.UART_BAUDRATE {115200} \
   CONFIG.UART_BOARD_INTERFACE {Custom} \
   CONFIG.USE_BOARD_FLOW {true} \
   CONFIG.USE_GPI1 {1} \
   CONFIG.USE_GPI2 {1} \
   CONFIG.USE_GPI3 {1} \
   CONFIG.USE_GPI4 {1} \
   CONFIG.USE_GPO1 {1} \
   CONFIG.USE_GPO2 {1} \
   CONFIG.USE_PIT1 {1} \
   CONFIG.USE_UART_RX {1} \
   CONFIG.USE_UART_TX {1} \
 ] $mcs0

  # Create interface connections
  connect_bd_intf_net -intf_net mcs0_GPIO3 [get_bd_intf_ports dip_switches_4bits] [get_bd_intf_pins mcs0/GPIO3]
  connect_bd_intf_net -intf_net mcs0_GPIO4 [get_bd_intf_ports push_buttons_5bits] [get_bd_intf_pins mcs0/GPIO4]
  connect_bd_intf_net -intf_net microblaze_mcs_0_GPIO1 [get_bd_intf_ports gpio1] [get_bd_intf_pins mcs0/GPIO1]
  connect_bd_intf_net -intf_net microblaze_mcs_0_GPIO2 [get_bd_intf_ports gpio2] [get_bd_intf_pins mcs0/GPIO2]
  connect_bd_intf_net -intf_net microblaze_mcs_0_UART [get_bd_intf_ports uart] [get_bd_intf_pins mcs0/UART]

  # Create port connections
  connect_bd_net -net Clk_0_1 [get_bd_ports clk] [get_bd_pins mcs0/Clk]
  connect_bd_net -net mcs0_PIT1_Toggle [get_bd_ports pit1_toggle] [get_bd_pins mcs0/PIT1_Toggle]
  connect_bd_net -net reset_1 [get_bd_ports reset_n] [get_bd_pins mcs0/Reset]

  # Create address segments


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


