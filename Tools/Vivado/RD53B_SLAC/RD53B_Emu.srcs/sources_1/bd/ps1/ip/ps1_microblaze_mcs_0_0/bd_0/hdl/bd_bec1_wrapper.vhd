--Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Command: generate_target bd_bec1_wrapper.bd
--Design : bd_bec1_wrapper
--Purpose: IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity bd_bec1_wrapper is
  port (
    Clk : in STD_LOGIC;
    GPIO1_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO1_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO2_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO2_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO3_tri_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    GPIO4_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 );
    PIT1_Toggle : out STD_LOGIC;
    Reset : in STD_LOGIC;
    UART_rxd : in STD_LOGIC;
    UART_txd : out STD_LOGIC
  );
end bd_bec1_wrapper;

architecture STRUCTURE of bd_bec1_wrapper is
  component bd_bec1 is
  port (
    Clk : in STD_LOGIC;
    Reset : in STD_LOGIC;
    PIT1_Toggle : out STD_LOGIC;
    UART_rxd : in STD_LOGIC;
    UART_txd : out STD_LOGIC;
    GPIO1_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO1_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO2_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO2_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO3_tri_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    GPIO4_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  end component bd_bec1;
begin
bd_bec1_i: component bd_bec1
     port map (
      Clk => Clk,
      GPIO1_tri_i(31 downto 0) => GPIO1_tri_i(31 downto 0),
      GPIO1_tri_o(31 downto 0) => GPIO1_tri_o(31 downto 0),
      GPIO2_tri_i(31 downto 0) => GPIO2_tri_i(31 downto 0),
      GPIO2_tri_o(31 downto 0) => GPIO2_tri_o(31 downto 0),
      GPIO3_tri_i(3 downto 0) => GPIO3_tri_i(3 downto 0),
      GPIO4_tri_i(4 downto 0) => GPIO4_tri_i(4 downto 0),
      PIT1_Toggle => PIT1_Toggle,
      Reset => Reset,
      UART_rxd => UART_rxd,
      UART_txd => UART_txd
    );
end STRUCTURE;
