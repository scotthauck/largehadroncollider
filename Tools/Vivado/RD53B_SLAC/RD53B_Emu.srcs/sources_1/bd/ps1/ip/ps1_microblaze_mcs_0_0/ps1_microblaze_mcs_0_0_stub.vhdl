-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Mon Nov 11 17:23:42 2019
-- Host        : RATBAG running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top ps1_microblaze_mcs_0_0 -prefix
--               ps1_microblaze_mcs_0_0_ ps1_microblaze_mcs_0_0_stub.vhdl
-- Design      : ps1_microblaze_mcs_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7k160tfbg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity ps1_microblaze_mcs_0_0 is
  Port ( 
    Clk : in STD_LOGIC;
    Reset : in STD_LOGIC;
    PIT1_Toggle : out STD_LOGIC;
    UART_rxd : in STD_LOGIC;
    UART_txd : out STD_LOGIC;
    GPIO1_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO1_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO2_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO2_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO3_tri_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    GPIO4_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 )
  );

end ps1_microblaze_mcs_0_0;

architecture stub of ps1_microblaze_mcs_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "Clk,Reset,PIT1_Toggle,UART_rxd,UART_txd,GPIO1_tri_i[31:0],GPIO1_tri_o[31:0],GPIO2_tri_i[31:0],GPIO2_tri_o[31:0],GPIO3_tri_i[3:0],GPIO4_tri_i[4:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "bd_bec1,Vivado 2017.4";
begin
end;
