-- (c) Copyright 1995-2019 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:ip:microblaze_mcs:3.0
-- IP Revision: 7

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY ps1_microblaze_mcs_0_0 IS
  PORT (
    Clk : IN STD_LOGIC;
    Reset : IN STD_LOGIC;
    PIT1_Toggle : OUT STD_LOGIC;
    UART_rxd : IN STD_LOGIC;
    UART_txd : OUT STD_LOGIC;
    GPIO1_tri_i : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    GPIO1_tri_o : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    GPIO2_tri_i : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    GPIO2_tri_o : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    GPIO3_tri_i : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    GPIO4_tri_i : IN STD_LOGIC_VECTOR(4 DOWNTO 0)
  );
END ps1_microblaze_mcs_0_0;

ARCHITECTURE ps1_microblaze_mcs_0_0_arch OF ps1_microblaze_mcs_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF ps1_microblaze_mcs_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT bd_bec1 IS
    PORT (
      Clk : IN STD_LOGIC;
      Reset : IN STD_LOGIC;
      PIT1_Toggle : OUT STD_LOGIC;
      UART_rxd : IN STD_LOGIC;
      UART_txd : OUT STD_LOGIC;
      GPIO1_tri_i : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      GPIO1_tri_o : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      GPIO2_tri_i : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      GPIO2_tri_o : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      GPIO3_tri_i : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      GPIO4_tri_i : IN STD_LOGIC_VECTOR(4 DOWNTO 0)
    );
  END COMPONENT bd_bec1;
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF GPIO4_tri_i: SIGNAL IS "XIL_INTERFACENAME GPIO4, C_USE_GPO4 0, C_GPO4_SIZE 32, C_GPO4_INIT 0x00000000, C_USE_GPI4 1, C_GPI4_SIZE 5, C_GPI4_INTERRUPT 0, BOARD.ASSOCIATED_PARAM GPIO4_BOARD_INTERFACE";
  ATTRIBUTE X_INTERFACE_INFO OF GPIO4_tri_i: SIGNAL IS "xilinx.com:interface:gpio:1.0 GPIO4 TRI_I";
  ATTRIBUTE X_INTERFACE_PARAMETER OF GPIO3_tri_i: SIGNAL IS "XIL_INTERFACENAME GPIO3, C_USE_GPO3 0, C_GPO3_SIZE 32, C_GPO3_INIT 0x00000000, C_USE_GPI3 1, C_GPI3_SIZE 4, C_GPI3_INTERRUPT 0, BOARD.ASSOCIATED_PARAM GPIO3_BOARD_INTERFACE";
  ATTRIBUTE X_INTERFACE_INFO OF GPIO3_tri_i: SIGNAL IS "xilinx.com:interface:gpio:1.0 GPIO3 TRI_I";
  ATTRIBUTE X_INTERFACE_INFO OF GPIO2_tri_o: SIGNAL IS "xilinx.com:interface:gpio:1.0 GPIO2 TRI_O";
  ATTRIBUTE X_INTERFACE_PARAMETER OF GPIO2_tri_i: SIGNAL IS "XIL_INTERFACENAME GPIO2, C_USE_GPO2 1, C_GPO2_SIZE 32, C_GPO2_INIT 0x00000000, C_USE_GPI2 1, C_GPI2_SIZE 32, C_GPI2_INTERRUPT 0, BOARD.ASSOCIATED_PARAM GPIO2_BOARD_INTERFACE";
  ATTRIBUTE X_INTERFACE_INFO OF GPIO2_tri_i: SIGNAL IS "xilinx.com:interface:gpio:1.0 GPIO2 TRI_I";
  ATTRIBUTE X_INTERFACE_INFO OF GPIO1_tri_o: SIGNAL IS "xilinx.com:interface:gpio:1.0 GPIO1 TRI_O";
  ATTRIBUTE X_INTERFACE_PARAMETER OF GPIO1_tri_i: SIGNAL IS "XIL_INTERFACENAME GPIO1, C_USE_GPO1 1, C_GPO1_SIZE 32, C_GPO1_INIT 0x00000000, C_USE_GPI1 1, C_GPI1_SIZE 32, C_GPI1_INTERRUPT 0, BOARD.ASSOCIATED_PARAM GPIO1_BOARD_INTERFACE";
  ATTRIBUTE X_INTERFACE_INFO OF GPIO1_tri_i: SIGNAL IS "xilinx.com:interface:gpio:1.0 GPIO1 TRI_I";
  ATTRIBUTE X_INTERFACE_INFO OF UART_txd: SIGNAL IS "xilinx.com:interface:uart:1.0 UART TxD";
  ATTRIBUTE X_INTERFACE_PARAMETER OF UART_rxd: SIGNAL IS "XIL_INTERFACENAME UART, BOARD.ASSOCIATED_PARAM UART_BOARD_INTERFACE";
  ATTRIBUTE X_INTERFACE_INFO OF UART_rxd: SIGNAL IS "xilinx.com:interface:uart:1.0 UART RxD";
  ATTRIBUTE X_INTERFACE_PARAMETER OF Reset: SIGNAL IS "XIL_INTERFACENAME RST.Reset, POLARITY ACTIVE_HIGH, BOARD.ASSOCIATED_PARAM RESET_BOARD_INTERFACE";
  ATTRIBUTE X_INTERFACE_INFO OF Reset: SIGNAL IS "xilinx.com:signal:reset:1.0 RST.Reset RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF Clk: SIGNAL IS "XIL_INTERFACENAME CLK.Clk, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN ps1_Clk_0, ASSOCIATED_ASYNC_RESET Reset, BOARD.ASSOCIATED_PARAM CLK_BOARD_INTERFACE";
  ATTRIBUTE X_INTERFACE_INFO OF Clk: SIGNAL IS "xilinx.com:signal:clock:1.0 CLK.Clk CLK";
BEGIN
  U0 : bd_bec1
    PORT MAP (
      Clk => Clk,
      Reset => Reset,
      PIT1_Toggle => PIT1_Toggle,
      UART_rxd => UART_rxd,
      UART_txd => UART_txd,
      GPIO1_tri_i => GPIO1_tri_i,
      GPIO1_tri_o => GPIO1_tri_o,
      GPIO2_tri_i => GPIO2_tri_i,
      GPIO2_tri_o => GPIO2_tri_o,
      GPIO3_tri_i => GPIO3_tri_i,
      GPIO4_tri_i => GPIO4_tri_i
    );
END ps1_microblaze_mcs_0_0_arch;
