# RD53-B Emulator #

### REPOSITORY HIERARCHY
<pre>

1. Documents
    |--Diagrams
    |--ILA
    |--KC705
    |--RD53
        |--RD53A specific documents
        |--Students thesis
    |--Schematics
        |--Board SLAC
    |--Spec
    |--Tutorial
      |--ILA
      |--KC705
      |--ModelSim
      |--Vivado
    |--Xilinx
2. Src
3. Testbench
4. Constraints
5. Tools
    |--ModelSim
    |--Vivado
        |--Back-up bit files
        |--RD53B_SLAC 


### Contributors ###
- Niharika Mittal (mittal77@uw.edu)
- Donavan Erickson (donavan2@uw.edu) 
- Geoff Jones (geoffhjones@msn.com)
- Tony Faubert


### Description ###
- This is a firmware RD53B emulator. The emulator has currently been set up to communicate properly 
  with the YARR firmware (located here https://github.com/Yarr/Yarr-fw). 
- Emuator is in a process to communicate successfully with FELIX firmware.
- The main pieces of currently working functionality are listed below.
    - Reading and writing of global registers.
    - Trigger commands will output clumps of trigger data. Trigger data that correctly comes out gets printed on the terminal.
    - Information on the installation of YARR and using YARR can be found here https://yarr.readthedocs.io/en/latest/.
    - Clock is provided as a separate input signal. 
    - A block diagram of the emulator as a system can be seen in documents/diagrams/emulator_block_diagram.PNG
           (https://gitlab.com/scotthauck/largehadroncollider/-/blob/master/Documents/Diagrams/emulator_block_diagram.PNG).
                      

FOR ALL THE INFORMATION REGARDING THE HARDWARE AND SOFTWARE CONNECTIONS AND SETUP, 
LOOK AT: Emulator_connections_with_various_readout_systems.pdf present on the home page of the repository.




    
